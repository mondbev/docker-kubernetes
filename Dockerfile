FROM nginxinc/nginx-unprivileged:1.20-alpine-perl

COPY index.html /usr/share/nginx/html 
EXPOSE 8080
